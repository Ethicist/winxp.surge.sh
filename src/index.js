import React from "react";
import { createRoot } from 'react-dom/client';
import App from "./App";

import "core-js/stable";
import "regenerator-runtime/runtime";

import "./styles.css";
import "xp.css/dist/98.css";

const container = document.getElementById('app');
const root = createRoot(container);
root.render(<App name="jokes.exe" />);