# [Good old dad jokes](https://winxp.surge.sh/)

![winxp](winxp.png?raw=true "winxp")

Батя грит, малаца, хорошо сделали!

Demo: [https://winxp.surge.sh/](https://winxp.surge.sh/)

## Building and running on localhost

Install dependencies:

```sh
yarn install
```

To run in hot module reloading mode:

```sh
yarn start
```

To create a production build:

```sh
yarn build
```

To create a development build:

```sh
yarn build-dev
```

## Running

Open the file `dist/index.html` in your browser

## Credits

[React typing effect](https://github.com/lamyfarai/react-typing-effect)
[React draggable](https://github.com/react-grid-layout/react-draggable)